
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.*;

public class Interfaz extends JFrame {

    JMenuBar menu;
    JMenu op, me,puntos;
    JMenuItem i1, i2, i3;
    int ancho = 530, alto = 600;
    JButton b1, b2, b3, b4,b5;
    JPanel p1, p2;
    JLabel px, py;
    JTextField tx, ty;
    Canvas f;

    private void inicializando() {
        b1 = new JButton("Cuadro");
        b2 = new JButton("Esfera");
        b3 = new JButton("Triangulo");
      //  b4 = new JButton("Linea");
        b5 = new JButton("Poligono");
        
        px = new JLabel("Puntos X:");
        py = new JLabel("Puntos Y:");
        tx = new JTextField("Punto X", 15);
        ty = new JTextField("Punto Y", 15);

        p1 = new JPanel();
        p2 = new JPanel();

        menu = new JMenuBar();

        op = new JMenu("");
        me = new JMenu("Informacion");
        puntos=new JMenu("");
        i1 = new JMenuItem("");// OP                
        //ME 
        //i2 = new JMenuItem("Informacion De Usuarios");
        i3 = new JMenuItem("Informacion De Los Controles");              
        f = new Canvas(this);

    }

    public Interfaz() {
        setTitle("Mini CAD - Adrian Zayas Viveros");
        inicializando();
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        int y = pantalla.height;
        int x = pantalla.width;
        setBounds((x - ancho) / 2, ((y - alto) / 2), ancho, alto);
        setLayout(new BorderLayout());
        setJMenuBar(menu);

        final GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);

        GLCanvas canvass = new GLCanvas(capabilities);

        canvass.addGLEventListener(f);
        canvass.addKeyListener(f);
        canvass.addMouseListener(f);
        canvass.addMouseMotionListener(f);        
        //canvass.setSize(700, 600);
        canvass.setSize(500, 500);
        
        f.add(canvass);
        FPSAnimator animator = new FPSAnimator(canvass, 300, true);
        animator.start();

//        menu.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);        
        p1.add(b1);
        p1.add(b2);
        p1.add(b3);
        //p1.add(b4);
        p1.add(b5);

        p2.add(px);
        p2.add(tx);
        p2.add(py);
        p2.add(ty);

        //op.add(i1);
        //me.add(i2);
        me.add(i3);
        menu.add(op);//JMENUBAR agregando JMENU
        menu.add(me);//JMENUBAR agregando JMENU 
        menu.add(puntos);
        add(p1, BorderLayout.NORTH);
        add(f, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);

        setDefaultCloseOperation(3);
        setVisible(true);
    }

}//JFrame 
