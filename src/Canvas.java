import Ayuda.panelAyudaControl;
import Figuras.Cuadrado;
import Figuras.Esfera;
import Figuras.Poligono;
import Figuras.Triangulo;
import com.jogamp.opengl.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.*;

/**
 *
 * @author
 */
public class Canvas extends JPanel implements GLEventListener, KeyListener, ActionListener, MouseListener, MouseMotionListener {

    GL2 gl;
    GLU glu = new GLU();
    GLUT glut = new GLUT();
    float rotar = 0f;
    float teclaX = 0, teclaY = 0, teclaZ = 50f;
    float direccionX = 52f, direccionY = 83, direccionZ = -101f;//DX52.0 DY 83.0 DZ -101.0//
    float e = 0.0f;
    Interfaz interfaz;
    int permiso = -1, conClick = 0;
    double x1 = 0, y1 = 0, x2 = 0, y2 = 0, x3 = 0, y3 = 0;
    double puntx = 0, punty = 0;
    double pli = 0, pli2 = 0;//cuadrado
    double pesx = 0, pesy = 0;//esfera
    double pttx = 0, ptty = 0;//triangulo
    double ppx = 0, ppy = 0;//poligono
    double plinea = 0, plinea2 = 0;
    boolean PermisoCuadro = false, PermisoTriangulo = false, PermisoEsfera = false, PermisoLinea = false, PermisoPoligono = false;
    boolean ctrlc = false, ctrlt = false, ctrlL = false, ctrlp = false, ctrle = false;
    boolean shiefT = false, shiefR = false, shiefE = false;
    //Traslaciones de ls figuras
    float ctx = 0, cty = 0, ttx = 0, tty = 0, etx = 0, ety = 0, ptx = 0, pty = 0, ltx = 0, lty = 0;
    //Rotaciones de las figuras 
    float crx = 0, cry = 0, trx = 0, trry = 0, erx = 0, ery = 0, prx = 0, pry = 0, lrx = 0, lry = 0;
    //Escalaciones de las figuras
    float cex = 0, cey = 0, tex = 0, tey = 0, eex = 0, eey = 0, pex = 0, pey = 0, lex = 0, ley = 0;
    int ic = 0, veces = 0;
    Poligono poli;
    Cuadrado cu;
    Esfera esf;
    Triangulo tri;
    public Canvas(Interfaz in) {
        this.interfaz = in;
        interfaz.b1.addActionListener(this);
        interfaz.b2.addActionListener(this);
        interfaz.b3.addActionListener(this);
        interfaz.b5.addActionListener(this);
        //interfaz.i2.addActionListener(this);
        interfaz.i3.addActionListener(this);
    }

    public void plano(GL2 gl) {
        //gl.glLineWidth(12f);//TODO EN -700 HASTA 700 SEGUN EL TAMANSHO DEL CANVASS
        gl.glBegin(GL2.GL_LINES);
        //Color para el eje X
        gl.glColor3f(0.0f, 0.5f, 1.0f);
        gl.glVertex3f(-500f, 0, 0);// eje X-
        gl.glVertex3f(500f, 0, 0);// eje X+
        int cont2 = 0;
        for (int i = -500; i <= 500; i += 10) {

            if (cont2 % 100 == 0) {
                gl.glVertex3f(i, 15f, 0);// eje X+    
                gl.glVertex3f(i, -15f, 0);// eje X-    
            } else {
                gl.glVertex3f(i, 5f, 0);// eje X+    
                gl.glVertex3f(i, -5f, 0);// eje X-    
            }
            cont2 += 10;
        }
        cont2 = 0;
        
        //Color para el eje Y
        gl.glColor3f(1.0f, 0.5f, 0.0f);
        gl.glVertex3f(0, -500f, 0);// EJE Y-
        gl.glVertex3f(0, 500f, 0);// eje Y+
        
        for (int i = -500; i <= 500; i += 10) {
            if (cont2 % 100 == 0) {
                gl.glVertex3f(15f, i, 0);// eje Y+    
                gl.glVertex3f(-15f, i, 0);// eje Y-    
            } else {
                gl.glVertex3f(5f, i, 0);// eje Y+    
                gl.glVertex3f(-5f, i, 0);// eje Y-    
            }
            cont2 += 10;
        }
        gl.glEnd();
        gl.glFlush();
    }

    @Override
    public void init(GLAutoDrawable glad) {
        glut = new GLUT();
        gl = glad.getGL().getGL2();
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        final GL2 gl = drawable.getGL().getGL2();
        GLU flu = new GLU();
        final GLUT glut = new GLUT();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glTranslatef(-50f, -50f, 0);
        movimientocamara(flu, drawable);
        plano(gl);

        if (PermisoCuadro == true) {
            gl.glTranslated(pli + ctx, pli2 + cty, 0);
            gl.glRotatef(crx - cry, .5f, 1f, 0);
            gl.glScaled(20 + cex, 20 + cey, 5);
            gl.glColor3f(1f, 1f, 1f);
            cu = new Cuadrado(gl);
            gl.glLoadIdentity();
        }
        if (PermisoTriangulo == true) {
            gl.glTranslated(pttx + ttx, ptty + tty, 0);
            gl.glRotatef(trx - trry, .5f, 1f, 0);
            gl.glScaled(10 + tex, 10 + tey, 5);
            gl.glColor3f(1f, 1f, 1f);
            tri = new Triangulo(gl);
            gl.glLoadIdentity();
        }

        if (PermisoEsfera == true) {
            gl.glTranslated(pesx + etx, pesx + ety, 0);
            gl.glRotatef(erx - ery, .5f, 1f, 0);
            gl.glScaled(5 + eex, 5 + eey, 2);
            gl.glColor3f(.9f, .5f, .1f);
            esf = new Esfera(glut);
            gl.glLoadIdentity();
        }
        if (PermisoPoligono == true) {
            gl.glTranslated(ptx + ppx, ppy + pty, 0);
            gl.glRotatef(prx - pry, .5f, 1f, 0);
            gl.glScaled(5 + pex, 5 + pey, 5);
            gl.glColor3f(7f, 20f, 14f);
            poli = new Poligono(gl);
            gl.glLoadIdentity();
        }

        gl.glFlush();

        gl.glLoadIdentity();
    }

    public void movimientocamara(GLU glu, GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glRotatef(teclaX, .5f, 0, 0);
        gl.glRotatef(teclaY, 0f, .5f, 0.0f);
//               DIRECCIONES        X Y Z                                 
        glu.gluLookAt(0.0, 0.0, 5.0, 0, 0, 0, 0.0, 1.0, 0.0);
        gl.glTranslatef(direccionX, direccionY, direccionZ);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        final GL2 gl = drawable.getGL().getGL2();

        if (height <= 0) {
            height = 1;
        }

        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        //180  
        glu.gluPerspective(150f, h, 1, 1000);
        glu.gluLookAt(0, 0, 40, 0, 0, 0, 0, 1, 0);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (interfaz.b1 == e.getSource()) {//Boton cuadro
            permiso = 1;
            System.out.println("CUBO");
        }
        if (interfaz.b2 == e.getSource()) {//Boton esfera
            permiso = 2;
            System.out.println("ESFERA");
        }
        if (interfaz.b3 == e.getSource()) {//Boton Triangulo
            permiso = 3;
            System.out.println("TRIANGULO");
        }
        if (interfaz.b5 == e.getSource()) {//Boton Poligono
            permiso = 5;
            System.out.println("POLIGONO");
        }
        if (interfaz.i3 == e.getSource()) {
            panelAyudaControl cdf = new panelAyudaControl();
            cdf.setVisible(true);
            cdf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        int X = e.getX();
        int Y = e.getY();
        int Xm = 250, Ym = 235;
        int x = (X - Xm), y = Ym - Y;
        puntx = x * 2;
        punty = y * 2;
        interfaz.px.setText("Puntos X:");
        interfaz.tx.setText("" + puntx);
        interfaz.py.setText("Puntos Y:");
        interfaz.ty.setText("" + punty);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int X = e.getX();
        int Y = e.getY();
        int Xm = 250, Ym = 235;
        int x = X - Xm;
        int y = Ym - Y;
        int xt = x * 2, yt = y * 2;
        if (permiso == 4) {
            conClick++;
        }

        switch (permiso) {
            case 1:
                pli = ((x * 2));
                pli2 = ((y * 2));
                PermisoCuadro = true;
                break;
            case 2:
                pesx = (x * 2);
                pesy = (y * 2);
                PermisoEsfera = true;
                interfaz.px.setText("CLICL EN X:");
                interfaz.tx.setText("" + xt);
                interfaz.py.setText("CLICK EN Y:");
                interfaz.ty.setText("" + yt);
                break;
            case 3:
                pttx = x * 2;
                ptty = y * 2;
                PermisoTriangulo = true;
                interfaz.px.setText("CLICL EN X:");
                interfaz.tx.setText("" + xt);
                interfaz.py.setText("CLICK EN Y:");
                interfaz.ty.setText("" + yt);
                break;
            case 5:
              ptx=   x * 2;
                pty=y * 2;
                PermisoPoligono = true;
                interfaz.px.setText("CLICL EN X:");
                interfaz.tx.setText("" + xt);
                interfaz.py.setText("CLICK EN Y:");
                interfaz.ty.setText("" + yt);
                break;

            default:
                JOptionPane.showMessageDialog(null, "CLICKE ALGUNA FIGURA PARA PODER EMPEZAR",
                        "ESCOJA ALGUNA FIGURA POR FAVOR", JOptionPane.WARNING_MESSAGE);
                break;
        }//CASE           
    }//MOUSECLICKED


    @Override
    public void keyPressed(KeyEvent e) {
        int tecla = e.getKeyCode();
        float p;

        //activar con el cubo o cuadrado
        if (e.isControlDown() && tecla == KeyEvent.VK_C) {
            if (ic % 2 == 0) {
                System.out.println("Ctrl C Activado");
                ctrlc = true;
                ctrlt = false;
                ctrlL = false;
                ctrlp = false;
                ctrle = false;
                interfaz.puntos.setText("Figura: " + "Cubo" + " Transformacion:");               
                ic++;
            } else {
                System.out.println("Ctrl c Desactivado");
                ctrlc = false;
                ic = 0;
            }
        }
        //activa con el triangulo
        if (e.isControlDown() && tecla == KeyEvent.VK_T) {
            if (ic % 2 == 0) {
                System.out.println("Ctrl T Activado");
                ctrlt = true;
                ctrlc = false;
                ctrlL = false;
                ctrlp = false;
                ctrle = false;
                interfaz.puntos.setText("Figura: " + "Triangulo" + " Transformacion:");
                ic++;
            } else {
                System.out.println("Ctrl T Desactivado");
                ctrlt = false;
                ic = 0;
            }
        }
        //Poligono
        if (e.isControlDown() && tecla == KeyEvent.VK_P) {
            if (ic % 2 == 0) {
                System.out.println("Ctrl P Activado");
                ctrlp = true;                          
                ctrlt = false;
                ctrlL = false;
                ctrlc = false;
                ctrle = false;
                interfaz.puntos.setText("Figura: " + "Poligono" + " Transformacion:");
                ic++;
            } else {
                System.out.println("Ctrl P Desactivado");
                ctrlp = false;
                ic = 0;
            }
        }
        //Esfera
        if (e.isControlDown() && tecla == KeyEvent.VK_E) {
            if (ic % 2 == 0) {
                System.out.println("Ctrl E Activado");
                ctrle = true;
                ctrlt = false;
                ctrlL = false;
                ctrlp = false;
                ctrlc = false;
                interfaz.puntos.setText("Figura: " + "Esfera" + " Transformacion:");
                ic++;
            } else {
                System.out.println("Ctrl E Desactivado");
                ctrle = false;
                ic = 0;
            }
        }

        if (tecla == KeyEvent.VK_X) {
            direccionX += 1.0f;
        }
        if (tecla == KeyEvent.VK_C) {
            direccionX -= 1.0f;
        }
        if (tecla == KeyEvent.VK_S) {
            direccionY += 1.0f;
        }
        if (tecla == KeyEvent.VK_W) {
            direccionY -= 1.0f;
        }
        if (tecla == KeyEvent.VK_Z) {
            direccionZ += 1.0f;
        }
        if (tecla == KeyEvent.VK_A) {
            direccionZ -= 1.0f;
        }

        if (tecla == KeyEvent.VK_ESCAPE) {
            //REINICIAR
            permiso=-1;
            direccionX = 52f;
            direccionY = 83;
            direccionZ = -101f;//DX52.0 DY 83.0 DZ -101.0//
            PermisoCuadro = false;
            PermisoTriangulo = false;
            PermisoEsfera = false;
            PermisoLinea = false;
            PermisoPoligono = false;
            ctrlc = false;
            ctrlt = false;
            ctrlL = false;
            ctrlp = false;
            ctrle = false;
            shiefT = false;
            shiefR = false;
            shiefE = false;
            //Traslaciones de ls figuras
            ctx = 0;
            cty = 0;
            ttx = 0;
            tty = 0;
            etx = 0;
            ety = 0;
            ptx = 0;
            pty = 0;
            ltx = 0;
            lty = 0;
            //Rotaciones de las figuras 
            crx = 0;
            cry = 0;
            trx = 0;
            trry = 0;
            erx = 0;
            ery = 0;
            prx = 0;
            pry = 0;
            lrx = 0;
            lry = 0;
            //Escalaciones de las figuras
            cex = 0;
            cey = 0;
            tex = 0;
            tey = 0;
            eex = 0;
            eey = 0;
            pex = 0;
            pey = 0;
            lex = 0;
            ley = 0;
            ic = 0;
            veces = 0;
        }

        if (ctrlc == true || ctrlt == true || ctrlp == true || ctrle == true) {
            if (e.isShiftDown() == true && tecla == KeyEvent.VK_T) {
                //EN CASO DE QUE QUIERAS ACTIVAR OTRO MODO 
                if (shiefR == true || shiefE == true) {
                    shiefR = false;
                    shiefE = false;
                    System.out.println("Desctivado Rotacion");
                    System.out.println("Desctivado Escalacion");
                    veces = 0;
                }
                if (veces % 2 == 0) {
                    shiefT = true;
                    veces++;
                    System.out.println("Activado Traslacion");

                } else {
                    shiefT = false;
                    veces = 0;
                }
            }
            if (e.isShiftDown() == true && tecla == KeyEvent.VK_R) {
                //EN CASO DE QUE QUIERAS ACTIVAR OTRO MODO 
                if (shiefT == true || shiefE == true) {
                    shiefT = false;
                    shiefE = false;
                    System.out.println("Desctivado Escalacion");
                    System.out.println("Desctivado Traslacion");
                    veces = 0;
                }
                if (veces % 2 == 0) {
                    shiefR = true;
                    System.out.println("Activado Rotacion");
                    veces++;
                } else {
                    shiefR = false;
                    veces = 0;
                }
            }
            if (e.isShiftDown() == true && tecla == KeyEvent.VK_E) {
                //EN CASO DE QUE QUIERAS ACTIVAR OTRO MODO 
                if (shiefT == true || shiefR == true) {
                    shiefT = false;
                    shiefR = false;
                    System.out.println("Desctivado Rotacion");
                    System.out.println("Desctivado Traslacion");
                    veces = 0;
                }
                if (veces % 2 == 0) {
                    shiefE = true;
                    System.out.println("Activado Escalacion");
                    veces++;
                } else {
                    shiefE = false;
                    veces = 0;
                }
            }
        }

        if (shiefE == true) {//SHIEF E
            if (ctrlc == true) {
                if (e.getKeyChar() == '-') {
                    cey++;
                }
                if (e.getKeyChar() == '*') {
                    cex++;
                }
                if (e.getKeyChar() == '+') {
                    cey--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    cex--;
                }
                System.out.println("CUADRO Escalando "
                        + " X=" + cex + " Y=" + cey);
            }//Activado por cubo y el tipo de transformacion
            if (ctrlt == true) {
                if (e.getKeyChar() == '-') {
                    tey++;
                }
                if (e.getKeyChar() == '*') {
                    tex++;
                }
                if (e.getKeyChar() == '+') {
                    tey--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    tex--;
                }
                System.out.println("TRIANGULO Escalando "
                        + " X=" + tex + " Y=" + tey);
            }
            if (ctrle == true) {
                if (e.getKeyChar() == '-') {
                    eey++;
                }
                if (e.getKeyChar() == '*') {
                    eex++;
                }
                if (e.getKeyChar() == '+') {
                    eey--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    eex--;
                }
                System.out.println("ESFERA Escalando "
                        + " X=" + eex + " Y=" + eey);
            }

            if (ctrlp == true) {
                if (e.getKeyChar() == '-') {
                    pey++;
                }
                if (e.getKeyChar() == '*') {
                    pex++;
                }
                if (e.getKeyChar() == '+') {
                    pey--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    pex--;
                }
                System.out.println("Poligono Escalacion "
                        + " X=" + pex + " Y=" + pey);
            }

        }//shief ESCLACION

        if (shiefT == true) {//SHIEF TRASLACION
            if (ctrlc == true) {
                if (e.getKeyChar() == '-') {
                    cty++;
                }
                if (e.getKeyChar() == '*') {
                    ctx++;
                }
                if (e.getKeyChar() == '+') {
                    cty--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    ctx--;

                    System.out.println("CUADRO Traslacion "
                            + " X=" + ctx + " Y=" + cty);
                }
            }//Activado por cubo y el tipo de transformacion
            if (ctrlt == true) {
                if (e.getKeyChar() == '-') {
                    tty++;
                }
                if (e.getKeyChar() == '*') {
                    ttx++;
                }
                if (e.getKeyChar() == '+') {
                    tty--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    ttx--;
                }
                System.out.println("TRIANGULO Traslacion "
                        + " X=" + ttx + " Y=" + tty);
            }
            if (ctrle == true) {
                if (e.getKeyChar() == '-') {
                    ety++;
                }
                if (e.getKeyChar() == '*') {
                    etx++;
                }
                if (e.getKeyChar() == '+') {
                    ety--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    etx--;
                }
                System.out.println("ESFERA Traslacion "
                        + " X=" + etx + " Y=" + ety);
            }

            if (ctrlp == true) {
                if (e.getKeyChar() == '-') {
                    pty++;
                }
                if (e.getKeyChar() == '*') {
                    ptx++;
                }
                if (e.getKeyChar() == '+') {
                    pty--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    ptx--;
                }
                System.out.println("Poligono Traslacion "
                        + " X=" + ptx + " Y=" + pty);
            }

        }//SHIEF TRASLACION

        if (shiefR == true) {//ShiefR
            if (ctrlc == true) {
                if (e.getKeyChar() == '-') {
                    cry++;
                }
                if (e.getKeyChar() == '*') {
                    crx++;
                }
                if (e.getKeyChar() == '+') {
                    cry--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    crx--;
                }
                System.out.println("CUADRO ROTACION "
                        + " X=" + crx + " Y=" + cry);
            }//Activado por cubo y el tipo de transformacion

            if (ctrlt == true) {
                if (e.getKeyChar() == '-') {
                    trry++;
                }
                if (e.getKeyChar() == '*') {
                    trx++;
                }
                if (e.getKeyChar() == '+') {
                    trry--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    trx--;
                }
                System.out.println("TRIANGULO Rotacion "
                        + " X=" + trx + " Y=" + trry);
            }
            if (ctrle == true) {
                if (e.getKeyChar() == '-') {
                    ery++;
                }
                if (e.getKeyChar() == '*') {
                    erx++;
                }
                if (e.getKeyChar() == '+') {
                    ery--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    erx--;
                }
                System.out.println("ESFERA Rotacion "
                        + " X=" + erx + " Y=" + ery);
            }

            if (ctrlp == true) {
                if (e.getKeyChar() == '-') {
                    pry++;
                }
                if (e.getKeyChar() == '*') {
                    prx++;
                }
                if (e.getKeyChar() == '+') {
                    pry--;
                }
                if (tecla == KeyEvent.VK_DIVIDE) {
                    prx--;
                }
                System.out.println("Poligono Rotacion "
                        + " X=" + prx + " Y=" + pry);
            }

        }//SHIEFF ROTACION
        //CUBO
        if (ctrlc == true) {
            if (shiefT == true) {
                interfaz.puntos.setText("Figura: " + "Cubo"
                        + " Transformacion: Traslacion");
            }
            if (shiefR == true) {
                interfaz.puntos.setText("Figura: " + "Cubo"
                        + " Transformacion: Rotacion");
            }
            if (shiefE == true) {
                interfaz.puntos.setText("Figura: " + "Cubo"
                        + " Transformacion: Escalada");
            }
        }
        ///ESFERA
        if (ctrle == true) {
            if (shiefT == true) {
                interfaz.puntos.setText("Figura: " + "Esfera"
                        + " Transformacion: Traslacion");
            }
            if (shiefR == true) {
                interfaz.puntos.setText("Figura: " + "Esfera"
                        + " Transformacion: Rotacion");
            }
            if (shiefE == true) {
                interfaz.puntos.setText("Figura: " + "Esfera"
                        + " Transformacion: Escalada");
            }
        }
        //POLIGONO
        if (ctrlp == true) {
            if (shiefT == true) {
                interfaz.puntos.setText("Figura: " + "Poligono"
                        + " Transformacion: Traslacion");
            }
            if (shiefR == true) {
                interfaz.puntos.setText("Figura: " + "Poligono"
                        + " Transformacion: Rotacion");
            }
            if (shiefE == true) {
                interfaz.puntos.setText("Figura: " + "Poligono"
                        + " Transformacion: Escalada");
            }
        }
        //TRIANGULO
        if (ctrlt == true) {
            if (shiefT == true) {
                interfaz.puntos.setText("Figura: " + "Triangulo"
                        + " Transformacion: Traslacion");
            }
            if (shiefR == true) {
                interfaz.puntos.setText("Figura: " + "Triangulo"
                        + " Transformacion: Rotacion");
            }
            if (shiefE == true) {
                interfaz.puntos.setText("Figura: " + "Triangulo"
                        + " Transformacion: Escalada");
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

//METODOS SIN UTILIZAR
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void dispose(GLAutoDrawable glad) {

    }
}
