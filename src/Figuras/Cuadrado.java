package Figuras;

import com.jogamp.opengl.*;

public class Cuadrado {
    GL2 gl;
    float rtx=0,rty=0,trx=0,Try=0;
    public Cuadrado(GL2 gl) { 
        gl.getGL().getGL2();
       
        //gl.glPushMatrix();
        //gl.glRotatef(40f, rtx, rty, 0);
        gl.glScaled(5, 5, 5);
        gl.glColor3d(100, 100, 90);
        
        gl.glBegin(GL2.GL_QUADS);
        gl.glColor3f(0f, 1f, 1f);
        //cara de enfrente 
          gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(1, 0, 0);
        gl.glVertex3f(1, 1, 0);
       gl.glVertex3f(0, 1, 0);        
//cara derecha
       gl.glVertex3f(1, 0, 0);
       gl.glVertex3f(1, 0, 1);
       gl.glVertex3f(1, 1, 1);
       gl.glVertex3f(1, 1, 0);
        //cara atras
       gl.glVertex3f(1, 0, 1);
       gl.glVertex3f(0, 0, 1);
       gl.glVertex3f(0, 1, 1);
       gl.glVertex3f(1, 1, 1);
        //cara izquierda
       gl.glVertex3f(0, 0, 1);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(0, 1, 1);
        //cara arriba
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(1, 1, 0);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(0, 1, 1);
       //cara de abajo
        gl.glColor3f(0f, 1f, 0f); 
       gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(1, 0, 0);
        gl.glVertex3f(1, 0, 1);
        gl.glVertex3f(0, 0, 1);                
        gl.glEnd();
        //gl.glPopMatrix();
    }
    
}
