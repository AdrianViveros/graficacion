/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

import com.jogamp.opengl.GL2;

/**
 *
 * @author Gaucho
 */
public class Triangulo {

    public Triangulo(GL2 gl) {
        gl.getGL().getGL2();
      gl.glScaled(10, 10, 5);
        gl.glBegin( GL2.GL_TRIANGLES );                
      
      gl.glColor3f( 1.0f, 0.0f, 0.0f );   // Red 
      gl.glVertex3f( 0.5f,0.7f,0.0f );    // Top 
      gl.glColor3f( 0.0f,1.0f,0.0f );     // blue 
      gl.glVertex3f( -0.2f,-0.50f,0.0f ); // Bottom Left 
      gl.glColor3f( 0.0f,0.0f,1.0f );     // green 
      gl.glVertex3f( 0.5f,-0.5f,0.0f );   // Bottom Right       
      gl.glEnd();    
      gl.glFlush();
    }
    
}
